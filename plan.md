# Representation des nombres en mémoire

## introduction

 - il faut un etat different pour chaque observation modélisée à laquelle on veut répondre différemment
 - beaucoup de phénomènes observable sont continus (infinité de possibilité)
 - il faudrait dans l'ordinateur une chose observable avec une infinité d'états
 - pour cela il faut soit une grandeur infinie, soit une grandeur continue bornée observable avec une précision infinie. Il faudrait donc pouvoir assumer un cout infini

 on va donc faire des concessions.

## Plan

  - 5mn : Intro + binaire (FT + OP)
  - 7mn : Entiers + flottants (OP)
  - 10mn : Rotozoom + virgule fixe (FT)
  - 6mn : Bresenham (OP)
  - 4mn : Bancaire / complexe (OP)
  - 12mn : 2600 (FT)
  - 1mn : pour la conclusion (FT + OP)

## Arrivée au binaire

 - combinaison d'états
 - N observables à M etats, on a le choix
 - babbage base 10 ? bit ?
 - math : 2.7 valeurs par observable, donc 2 ou 3 valeurs
 - transistors : 5V x 1mA = 5mW, 1mV x 5A aussi, mais 2.5A x 2.5V = 6.25W
 - Avec deux valeurs, besoin de moins de précision

 -> deux valeurs retenu

### groupées par combien ? un puissance de deux (4, 8, 16, 32, ...)

### quelle bijection nombre <-> représentation ?

  On pourrait faire ca :

0 000000
1 100000
2 110000
3 111000
4 111100
5 111110
6 011111
7 101111
8 110111
9 111011
 
 Mais on va plutot faire des trucs bien pour manipuler

#### Binaire

## Les entiers

  - Taille des paquets (8-bits, 16-bits, etc ...)
  - Représentation signée
    - Complément à 1
    - Complément à 2

## La virgule flottante

  - Imprécis mais suffisants pour la plupart des applications
  - Il faudrait parler d'approximation plutôt que de précision
    - simple précision : approximation à ~6 chiffres après la virgule
    - double précision : approximation à ~16 chiffres après la virgule
    - autres précisions : applications scientifiques spécifiques (i.e long double sur 80-bits et 128-bits)
  - Attention aux comparaisons d'égalité
  - Pourquoi on peut diviser par zéro => donne +/- l'infini, impossible avec les entiers car ne sait pas le représenter

## La virgule fixe

  - Se base sur des calcul entiers -> assez rapide
  - Limites de part la taille des paquets de bits (i.e Q16.16)
  - Utilisation dans les GPU pour la rasterisation
  - Application pratique dans le Rotozoom

### RotoZoom

#### implémentation de référence avec des nombres en virgule flottante

Implémentation en C++, utilisation de la virgule flottante en double précision :

```
struct tex2f
{
    tex2f()
        : u()
        , v()
    {
    }

    tex2f(double x, double y)
        : u(x)
        , v(y)
    {
    }

    double u;
    double v;
};

void render ( const int       w   /* image width        */
            , const int       h   /* image height       */
            , const double    cos /* angle cos          */
            , const double    sin /* angle sin          */
            , uint32_t*       dst /* destination buffer */
            , const uint32_t* src /* source texture     */ )
{
    const tex2f abscissa(+cos, +sin);
    const tex2f ordinate(-sin, +cos);

    tex2f texture;
    for(int y = 0; y < h; ++y) {
        const tex2f origin(texture);
        for(int x = 0; x < w; ++x) {
            const int u = (static_cast<unsigned int>(texture.u) % w);
            const int v = (static_cast<unsigned int>(texture.v) % h);
            *dst++ = src[w * v + u];
            texture.u += abscissa.u;
            texture.v += abscissa.v;
        }
        texture.u = (origin.u + ordinate.u);
        texture.v = (origin.v + ordinate.v);
    }
}
```

![RotoZoom 80° float](assets/rotozoom/RotoZoom_80_float.png "RotoZoom 80° float")

#### implémentation optimisée avec des nombres en virgule fixe

Implémentation en C++, utilisation de la virgule fixe Q16.16, architecture little-endian :

```
struct tex2i
{
    tex2i()
        : q()
    {
    }

    tex2i(int32_t x, int32_t y)
        : u(x)
        , v(y)
    {
    }

    union {
        struct {
            int64_t q;
        };
        struct {
            int32_t u;
            int32_t v;
        };
        struct {
            uint16_t ul;
            uint16_t uh;
            uint16_t vl;
            uint16_t vh;
        };
    };
};

void render ( const int       w   /* must be a power of two */
            , const int       h   /* must be a power of two */
            , const int32_t   cos /* Q16.16 fixed-point cos */
            , const int32_t   sin /* Q16.16 fixed-point sin */
            , uint32_t*       dst /* destination buffer     */
            , const uint32_t* src /* source texture         */ )
{
    const int   max_w = (w - 1);
    const int   max_h = (h - 1);
    const tex2i abscissa(+cos, +sin);
    const tex2i ordinate(-sin, +cos);

    tex2i texture;
    for(int y = h; y != 0; --y) {
        const tex2i origin(texture);
        for(int x = w; x != 0; --x) {
            const int u = (texture.uh & max_w);
            const int v = (texture.vh & max_h);
            *dst++ = src[w * v + u];
            texture.q += abscissa.q;
        }
        texture.q = (origin.q + ordinate.q);
    }
}
```

![RotoZoom 80° fixed](assets/rotozoom/RotoZoom_80_fixed.png "RotoZoom 80° fixed")

#### Différence entre les deux implémentations

Matérialisation de l'erreur graphique entre les deux résultats :

![RotoZoom 80° difference](assets/rotozoom/RotoZoom_80_difference.png "RotoZoom 80° difference")

Performances mesurées avec tables de sinus/cosinus précalculées :

  - En virgule flottante : moyenne de 1267 images par seconde.
  - En virgule fixe : moyenne de 1466 images par seconde.

Soit un écart de performance de ~16% mesuré sur une machine moderne dotée d'un CPU et FPU relativement performants (Intel(R) Core(TM) i7-8565U CPU @ 1.80GHz).

## Autres représentations
 
  - Proportionalité : algo de tracé de ligne de Bresenham -> algo incrémental avec intégration et répercussion de l'erreur
  - Finance : amount -> quatité argent / quantité produits -> prix moyen

## Le TIA (atari 2600)

   probleme : vite et pas cher

   Ripple counter

    - super rapide
    - on sait pas trop générer la coordonnées -> polynomial
    - 6502 pas assez rapide (64 possibilités) -> ajout d'un positionnement fin
    - code de gray / ripple counter
