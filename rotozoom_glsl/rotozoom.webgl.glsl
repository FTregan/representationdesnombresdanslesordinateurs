#version 300 es

#define PI 3.1415926535897932384626433832795

precision highp float;

uniform vec2 u_resolution;

uniform sampler2D texFond;
uniform sampler2D texMaela;
uniform sampler2D texScrolltext;

uniform float u_time;

layout(location = 0) out vec4 out_color; // out_color must be written in order to see anything

void main(void)
{
  vec2 xy = gl_FragCoord.xy / u_resolution * vec2(1., -1.);
  vec2 uv = gl_FragCoord.xy / u_resolution * 2. -1.;

  vec4 papierPx =
    texture(texFond, xy);

  float z = uv.y  * -0.127  + 0.22;
  vec2 uv2 = uv / z / 256.;
  vec2 zoomUv = uv2  * ( 1. + sin(u_time)/400.0);
  
  float rotAngle = mod(0.1 * u_time + 2., 2.0*PI);

  vec2 rotuv = vec2(
      -zoomUv.x * cos(rotAngle) - zoomUv.y * sin(rotAngle),
      -zoomUv.x * sin(rotAngle) + zoomUv.y * cos(rotAngle)
  );

  vec4 maelaPx = texture(texMaela, rotuv*100.);
  maelaPx = mix(vec4(1., 1., 1., 1.), maelaPx, 0.6 - uv.y / 2. );

  vec4 color = vec4(maelaPx.xyz * (1. - papierPx.w * 2.) + papierPx.xyz * (papierPx.w), 1.);

  vec2 scrolltextUv = uv * vec2(0.2, -4.) + vec2(u_time / 55.,  sin(uv.x * 6.- u_time*1.)/1.5);
  vec4 scrolltextPx = vec4(1.);
  if (scrolltextUv.y > 2. && scrolltextUv.y < 3.) {
    scrolltextPx = texture(texScrolltext, scrolltextUv);
  }

  out_color = maelaPx * papierPx * scrolltextPx;
}
