#version 410 core

#define PI 3.1415926535897932384626433832795

uniform float fGlobalTime; // in seconds
uniform vec2 v2Resolution; // viewport resolution (in pixels)
uniform float fFrameTime; // duration of the last frame, in seconds

uniform sampler1D texFFT; // towards 0.0 is bass / lower freq, towards 1.0 is higher / treble freq
uniform sampler1D texFFTSmoothed; // this one has longer falloff and less harsh transients
uniform sampler1D texFFTIntegrated; // this is continually increasing
uniform sampler2D texPreviousFrame; // screenshot of the previous frame
uniform sampler2D texMaela;
uniform sampler2D texCredits;

in vec2 out_texcoord;
layout(location = 0) out vec4 out_color; // out_color must be written in order to see anything

vec4 fromLinear(vec4 linearRGB)
{
	bvec4 cutoff = lessThan(linearRGB, vec4(0.0031308));
	vec4 higher = vec4(1.055)*pow(linearRGB, vec4(1.0/2.4)) - vec4(0.055);
	vec4 lower = linearRGB * vec4(12.92);

	return mix(higher, lower, cutoff);
}

void main(void)
{
	vec2 uv = out_texcoord;
	uv -= 0.5;
	uv /= vec2(v2Resolution.y / v2Resolution.x, 1);

    float bendAngle = 1.25 + cos(fGlobalTime*1.0)*0.0125*PI;
    float z = (1.0-uv.y) * tan(bendAngle);
    vec2 uv2 = uv / (z+1.0);
    // uv x -1..1 y -0.7..0.7
    vec2 zoomUv = uv2 * (sin(fGlobalTime)*1.0+10.5);
    float rotAngle = mod(fGlobalTime*0.2, 2.0*PI);
    // rotAngle 0..2pi
    vec2 rotuv = vec2(
        (-1.0) * zoomUv.x * cos(rotAngle) - zoomUv.y * sin(rotAngle),
        (-1.0) * zoomUv.x * sin(rotAngle) + zoomUv.y * cos(rotAngle)
    );
//    vec2 aspectCorrected = maela.yx ;
    
//  vec2 repRotUv = mod(aspectCorrected, 1.0);
  vec4 maelaPx = texture(texMaela, rotuv);
  vec4 creditsPx = texture(texCredits, out_texcoord / vec2(v2Resolution.y / v2Resolution.x, -1));
  vec4 plop = mix(maelaPx, creditsPx, creditsPx.a * 1.0);
	out_color = fromLinear(plop);
}