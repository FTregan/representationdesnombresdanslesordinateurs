# Rotozoom pour 486

## How to run

 - Start a DosBox (or better: 86Box with a 486Dx4-100)
 - Have Turbo Assembler 4.1 installed, with its `/bin` directory in path
 - make with `make` and run with `rz`

## My development setup

 - Code is edited with an IDE on windows (don't ask)
 - It's compiled an tested using DosBox 0.74-3, but then ran on 86Box 3.0 build 3333

### DosBox configuration

 - copy `dosbox/dosbox-0.74-3.conf` in your [dosbox configuration directory](https://www.dosbox.com/wiki/Dosbox.conf#Windows_Vista.2C_Windows_7_.26_Windows_8.x) (change `74-3` to the dosbow version you use)

 it's basically a default configuration, whith `cycles` incremented to `27600` (approximatively 486Dx2-66 speed) and these lines to mount the host directory (`c:\repres`), which you will need to change to your own :

```
mount c c:\repres
c:
set PATH=Z:\;C:\TASM\BIN\
```

### 86Box configuration

You will find my configuration file in `86Box\`. If you want to reuse it, copy it to your 86Box directory but you will need to edit the virtual hard drive path.

Machine:
 - Machine type: `i486 (Socket 168 and 1)`
 - Machine:` [ALi M1429] Olystar LIL1429`
 - CPU Type: `Intel iDX4`
 - Speed: `100`
 - Memory: `8MB`
 - Time synchronization: `Enable (local time)`

Display:
 - Video: `[VLB] S3 Trio64 (SPEA Mirage P64)`

Storage controllers:
 - HD and FD controllers: `Internal controller`
 - Cassette: unchecked
 - Hard disk: create a new default 20MB image. At first boot, you will need to go to the BIOS setting (`F1` after error message, or `del` before) and choose `autodetect hard drive` then save and exit

On first boot, you will need to insert MsDos 6.22 first disk and follow instructions to install dos to the hard drive.

Then install Turbo Assembler 4.1

To run several instances, make a subdirectories into 86Box named e.g. `dir1`, `dir2`, ... copy the `86box.cfg` in each directory, make multiple copy of the virtual hard drive file, edit the `86box.cfg` files to point each to a different VHD file, then run with `86box -P dir1`, `86box -P dir2`, ...

### AutoHotKey

To copy my source files from the host computer (where I code) and the VM drive, I use [PCemuFileInjector](https://github.com/viler-int10h/PCemuFileInjector), but the keyboard was too fast and caused my emulator to stall, so I modified the script and run it with [AutoHotKey](https://www.autohotkey.com/) (the script is in `86Bos`)

## Copyright

Picture by Maela Cosson, with permission:
 - http://tuserasunegourde.blogspot.com/
 - https://twitter.com/thegourde

Code by Fabien Tregan. Use, study, modify, duplicate, sell, ... do whatever you want with it as long as you don't hurt people.
